use std::io;
use io::Write;

fn main() -> Result<(), io::Error> {
	print!("Enter a string: ");
	io::stdout().flush()?;

	let mut line = String::new();
	io::stdin().read_line(&mut line)?;
	let input = line.trim_end();

	for character in input.bytes() {
		print!("{} ", character);
	}

	println!();

	Ok(())
}
